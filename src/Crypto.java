import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {

   static void fileProcessor(int cipherMode,String key,File inputFile,File outputFile){
       try {
    	     		
           Key secretKey = new SecretKeySpec(key.getBytes(), "AES");
           Cipher cipher = Cipher.getInstance("AES");
           cipher.init(cipherMode, secretKey);

           FileInputStream inputStream = new FileInputStream(inputFile);
           byte[] inputBytes = new byte[(int) inputFile.length()];
           inputStream.read(inputBytes);

           byte[] outputBytes = cipher.doFinal(inputBytes);

           FileOutputStream outputStream = new FileOutputStream(outputFile);
           outputStream.write(outputBytes);

           inputStream.close();
           outputStream.close();

       } catch (NoSuchPaddingException | NoSuchAlgorithmException
               | InvalidKeyException | BadPaddingException
               | IllegalBlockSizeException | IOException e) {
           e.printStackTrace();
       }
   }
   
   public static void crypteAllFile(String nomRepository)
   {
	   	File splitFiles = new File(nomRepository+"/");// Récupère tout les fichiers du dossier vidéos
		if (splitFiles.exists()) 
		{
			File[] files = splitFiles.getAbsoluteFile().listFiles();

			// On y trie dans l'ordre
			Arrays.sort(files);
			
			String key = "ZuFK5X2zdzba7qva";
		       
			for (File file : files) 
			{
				
				String nomVideo = file.getName();	

				//On récupère le nom et l'extension du fichier
				//String fileName = nomVideo.substring(0, nomVideo.lastIndexOf("."));
				//String ext = nomVideo.substring(nomVideo.lastIndexOf("."));
				
									
			    File inputFile = new File("SampleVideo/" + nomVideo);
			    File encryptedFile = new File("EncryptionResult/Crypted/"+nomVideo);
			    

			    try { 
			         Crypto.fileProcessor(Cipher.ENCRYPT_MODE,key,inputFile,encryptedFile);    
			         
			       } catch (Exception ex) {
			           System.out.println(ex.getMessage());
			           ex.printStackTrace();			
			       }
			}
		}
   }
   
   public static void decrypteAllFile(String nomRepository)
   {
	   	File splitFiles = new File(nomRepository+"/");// Récupère tout les fichiers du dossier vidéos
		if (splitFiles.exists()) 
		{
			File[] files = splitFiles.getAbsoluteFile().listFiles();

			// On y trie dans l'ordre
			Arrays.sort(files);
			
			String key = "ZuFK5X2zdzba7qva";
		       
			for (File file : files) 
			{
				
				String nomVideo = file.getName();	

				File inputFile = new File("EncryptionResult/Crypted/" + nomVideo);
				 					
			
				 File decryptedFile = new File("EncryptionResult/Decrypted/"+nomVideo);
				 
				 
				 
				    try { 
				         
				         Crypto.fileProcessor(Cipher.DECRYPT_MODE,key,inputFile,decryptedFile);  
				       
				         
				       } catch (Exception ex) {
				           System.out.println(ex.getMessage());
				           ex.printStackTrace();			
				       }		
				
				
			}
		}
   }
   
        

   public static void main(String[] args) {
	   
	   //Date dateDebut = new Date();
       String key = "ZuFK5X2zdzba7qva";
       File inputFile = new File("SampleVideo/100MO.mp4");
       File encryptedFile = new File("EncryptionResult/Crypted/100MO.mp4");
       File decryptedFile = new File("EncryptionResult/Decrypted/100MO.mp4");
       
              

       try { 
           Crypto.fileProcessor(Cipher.ENCRYPT_MODE,key,inputFile,encryptedFile);    
           Crypto.fileProcessor(Cipher.DECRYPT_MODE,key,encryptedFile,decryptedFile);  
           System.out.println("Success. Fichier sauvegardé dans le dossier 'EncryptionResult' ");
           
       } catch (Exception ex) {
           System.out.println(ex.getMessage());
           ex.printStackTrace();
       }
       
       //Pour crypter toutes les vidéos dans SampleVideo
       crypteAllFile("SampleVideo");
       
       
       //Pour décrypter toutes les vidéos dans EncryptionResult/Crypted
       decrypteAllFile("EncryptionResult/Crypted");
       			
       	//Pour tester le temps d'exécution
       	 
       	//Date dateFin = new Date();
       	//Formatage de la date
 		//DateFormat format = new SimpleDateFormat("HH:mm:ss");

 		//Affichage des Timers dans le bon format
 		//System.out.println("Début du décryptage -> " + format.format(dateDebut));
		//System.out.println("Fin du décryptage -> " + format.format(dateFin));   
       
  		
   }

}